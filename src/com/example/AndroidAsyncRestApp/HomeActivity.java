package com.example.AndroidAsyncRestApp;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.*;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class HomeActivity extends Activity {

    final String TAG = "AndroidAsyncRestApp";

    private Intent serviceIntent;
    private Messenger serviceMessenger;

    boolean mBounded;
    ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceDisconnected(ComponentName name) {
            mBounded = false;
            serviceMessenger = null;
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            //Toast.makeText(HomeActivity.this, "Service is connected", 1000).show();
            serviceMessenger = new Messenger(service);
            mBounded = true;
            Message message = Message.obtain(null, MessageTypes.ADD_ACTIVITY_HANDLER, new ClientHandle());
            try {
                serviceMessenger.send(message);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        serviceIntent = new Intent(this, BackgroundService.class);
        startService(serviceIntent);
    }

    @Override
    public void onPause(){
        super.onPause();
        unbindService(mConnection);
    }

    @Override
    public void onResume(){
        super.onResume();
        bindService(serviceIntent, mConnection, BIND_AUTO_CREATE);
    }

    public void onClickTestAsyncGet(View v){
        try {
            serviceMessenger.send(Message.obtain(null, MessageTypes.TEST_ASYNC_GET, ""));
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public class ClientHandle extends Handler {

        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case MessageTypes.ADD_ACTIVITY_HANDLER:
                    Log.d(TAG, "Activity:" + msg.obj.toString());
                    Toast.makeText(HomeActivity.this, "Messenger registered", 1000).show();
                    break;
                case MessageTypes.TEST_ASYNC_GET:
                    Toast.makeText(HomeActivity.this, "Async get received", 1000).show();
                    Log.d(TAG, "Response: " + msg.obj.toString());
                default:
                    break;
            }
            super.handleMessage(msg);
        }
    }
}
