package com.example.AndroidAsyncRestApp;

import android.app.Service;
import android.content.Intent;
import android.os.*;
import android.util.Log;
import android.widget.Toast;
import com.koushikdutta.async.http.AsyncHttpClient;
import com.koushikdutta.async.http.AsyncHttpResponse;

/**
 * User: akureniov
 * Date: 24.04.13
 * Time: 17:21
 */
public class BackgroundService extends Service {
    final String TAG = "AndroidAsyncRestApp";

    Looper mServiceLooper;
    ServiceHandler mServiceHandler;
    Messenger serviceMessenger;
    Messenger clientMessenger;

    @Override
    public void onCreate() {
        super.onCreate();
        HandlerThread thread = new HandlerThread("PeopleairServiceHandler",
                android.os.Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();
        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
        serviceMessenger = new Messenger(mServiceHandler);

    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    @Override
    public IBinder onBind(Intent intent) {
        return serviceMessenger.getBinder();
    }

    public class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MessageTypes.ADD_ACTIVITY_HANDLER:
                    try {
                        clientMessenger = new Messenger((Handler) msg.obj);
                        Log.d(TAG, "Service:" + msg.obj.toString());
                        clientMessenger.send(Message.obtain(null, MessageTypes.ADD_ACTIVITY_HANDLER, "Client messenger registered"));
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    break;
                case MessageTypes.TEST_ASYNC_GET:
                    AsyncHttpClient.getDefaultInstance().get("http://google.com/" + msg.obj.toString(), new AsyncHttpClient.StringCallback(){
                        @Override
                        public void onCompleted(Exception e, AsyncHttpResponse source, String result) {
                            if (e != null) {
                                e.printStackTrace();
                                return;
                            }
                            try {
                                clientMessenger.send(Message.obtain(null, MessageTypes.TEST_ASYNC_GET, result));
                            } catch (RemoteException e1) {
                                e1.printStackTrace();
                            }
                        }
                    });
                    break;
                default:
                    break;
            }
            super.handleMessage(msg);
        }
    }
}
