package com.example.AndroidAsyncRestApp;

/**
 * User: akureniov
 * Date: 24.04.13
 * Time: 17:24
 */
public final class MessageTypes {
    public static final int ADD_ACTIVITY_HANDLER = 1;
    public static final int TEST_ASYNC_GET = 2;
}

